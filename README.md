This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## docker

Inside project directory build docker image by command:

docker build . -t albums-webapp

Then run docker container by command:

docker run --rm -d -p 8080:8080 albums-webapp

check localhost:8080

## internet explorer

Due CRA 3.0 issue IE does not work with development server. To run it on IE, run production build from docker or with serve app:

npm run build
npm install -g serve
serve -s build
