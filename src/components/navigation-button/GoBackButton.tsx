import React from 'react';
import { useHistory } from 'react-router-dom';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

const GoBackButton: React.SFC = () => {
  const history = useHistory();

  return (
    <Tooltip title="Go back">
      <IconButton
        edge="start"
        color="inherit"
        aria-label="menu"
        onClick={() => history.goBack()}
      >
        <ArrowBackIcon />
      </IconButton>
    </Tooltip>
  );
};

export default GoBackButton;
