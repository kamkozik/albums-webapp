import React from 'react';
import { Link } from 'react-router-dom';
import { createStyles, makeStyles } from '@material-ui/styles';
import PhotoAlbumIcon from '@material-ui/icons/PhotoAlbum';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { List, Drawer, ListItem, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    list: {
      width: 200
    },
    listItem: {
      height: 50,
      '&:hover': {
        color: theme.palette.primary.main
      },
      color: '#000000'
    },
    link: {
      textDecoration: 'none'
    },
    icon: {
      color: 'inherit',
      '&:hover': {
        color: 'inherit'
      }
    }
  })
);

interface DrawerProps {
  open: boolean;
  onClose: (event: React.MouseEvent<HTMLAnchorElement>) => void;
}

const AppDrawer: React.SFC<DrawerProps> = ({ open, onClose }) => {
  const classes = useStyles();

  return (
    <Drawer open={open} onClose={onClose}>
      <List className={classes.list} aria-label="navigation items">
        <Link className={classes.link} onClick={onClose} to="/albums">
          <ListItem className={classes.listItem} button>
            <ListItemIcon className={classes.icon}>
              <PhotoAlbumIcon />
            </ListItemIcon>
            <ListItemText>{'Albums'}</ListItemText>
          </ListItem>
        </Link>
      </List>
    </Drawer>
  );
};

export default AppDrawer;
