import React, { useState, Fragment } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Box from '@material-ui/core/Box';
import AppDrawer from './AppDrawer';
import { Button, Hidden, makeStyles, Theme, createStyles } from '@material-ui/core';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    link: {
      textDecoration: 'none'
    },
    button: {
      color: 'white'
    }
  })
);

const AppHeader: React.FC = () => {
  const classes = useStyles();

  const [openAppDrawer, setOpenAppDrawer] = useState(false);
  const toggleDrawer = () => {
    setOpenAppDrawer(!openAppDrawer);
  };
  return (
    <Fragment>
      <AppBar position="fixed">
        <Toolbar component="nav">
          <Box display="flex" flex={1}>
            <Hidden smUp>
              <IconButton onClick={toggleDrawer} edge="start" color="inherit" aria-label="menu">
                <MenuIcon />
              </IconButton>
            </Hidden>
            <Hidden xsDown>
              <Link className={classes.link} to="/albums">
                <Button className={classes.button}>Albums</Button>
              </Link>
            </Hidden>
          </Box>
          <Box display="flex">
            <Typography variant="h6">Albums App</Typography>
          </Box>
          <Box display="flex" flex={1}></Box>
        </Toolbar>
      </AppBar>
      <AppDrawer open={openAppDrawer} onClose={toggleDrawer} />
    </Fragment>
  );
};

export default AppHeader;
