import React from 'react';
import { createStyles, makeStyles } from '@material-ui/styles';
import { Theme, Box } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    background: {
      backgroundColor: theme.palette.background.default,
      minHeight: '100vh',
      padding: theme.spacing(12, 8),
      [theme.breakpoints.down('sm')]: {
        padding: theme.spacing(12, 3)
      }
    }
  })
);

const BackgroundBox: React.SFC = ({ children }) => {
  const classes = useStyles();

  return <Box className={classes.background}>{children}</Box>;
};

export default BackgroundBox;
