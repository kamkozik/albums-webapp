import { combineReducers } from 'redux';
import { albumsListReducer } from '../containers/albums-list/+redux/';
import { albumDetailsReducer } from '../containers/album-details/+redux';
import { userDetailsReducer } from '../containers/users-details/+redux';

const rootReducer = combineReducers({
  albumsList: albumsListReducer,
  albumDetails: albumDetailsReducer,
  userDetails: userDetailsReducer
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
