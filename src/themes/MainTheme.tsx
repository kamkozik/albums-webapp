import { createMuiTheme } from '@material-ui/core/styles';
import { grey, blueGrey } from '@material-ui/core/colors';

const MainTheme = createMuiTheme({
  palette: {
    primary: blueGrey,
    background: {
      default: grey[100]
    }
  },
  shape: {
    borderRadius: 8
  }
});

export default MainTheme;
