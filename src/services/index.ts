import setupNetworking from './api-service';
import AlbumsListService from './albums-list-service';
import AlbumDetailsService from './album-details-service';
import UserDetailsService from './user-details-service';

export { setupNetworking, AlbumsListService, AlbumDetailsService, UserDetailsService };
