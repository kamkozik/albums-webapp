import axios from 'axios';

const setupNetworking = () => {
  axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com/';
  axios.defaults.timeout = 30000;
};

export default setupNetworking;
