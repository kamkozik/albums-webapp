import axios from 'axios';
import AlbumPhoto from '../models/AlbumPhoto.model';
import Album from '../models/AlbumsList.model';

const fetchPhotosList = (
  albumId: string | number
): Promise<Array<AlbumPhoto>> => {
  return axios
    .get('photos', {
      params: {
        albumId
      }
    })
    .then(
      response => {
        return Promise.resolve(response.data);
      },
      err => {
        return Promise.reject(err);
      }
    );
};

const fetchAlbumDetails = (id: string | number): Promise<Array<Album>> => {
  return axios
    .get('albums', {
      params: {
        id
      }
    })
    .then(
      response => {
        return Promise.resolve(response.data);
      },
      err => {
        return Promise.reject(err);
      }
    );
};

const AlbumDetailsService = {
  fetchPhotosList,
  fetchAlbumDetails
};

export default AlbumDetailsService;
