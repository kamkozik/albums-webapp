import axios from 'axios';
import { User } from '../models/User.model';

const fetchUsersDetails = (id: string | number): Promise<Array<User>> => {
  return axios
    .get('users', {
      params: {
        id
      }
    })
    .then(
      response => {
        return Promise.resolve(response.data);
      },
      err => {
        return Promise.reject(err);
      }
    );
};

const UsersDetailsService = {
  fetchUsersDetails
};

export default UsersDetailsService;
