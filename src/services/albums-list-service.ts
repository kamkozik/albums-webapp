import axios from 'axios';
import Album from '../models/AlbumsList.model';

const fetchAlbumsList = (): Promise<Array<Album>> => {
  return axios.get(`albums`).then(
    response => {
      return Promise.resolve(response.data);
    },
    err => {
      return Promise.reject(err);
    }
  );
};

const AlbumsListService = {
  fetchAlbumsList
};

export default AlbumsListService;
