import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import AppHeader from '../components/header/AppHeader';
import AlbumsListView from '../views/AlbumsListView';
import AlbumDetailsView from '../views/AlbumDetailsView';
import UserDetailsView from '../views/UserDetailsView';

function AppRouter() {
  return (
    <Router>
      <AppHeader />
      <Switch>
        <Route path="/album/:id" component={AlbumDetailsView} />
        <Route path="/user/:id" component={UserDetailsView} />
        <Route path="/" component={AlbumsListView} />
      </Switch>
    </Router>
  );
}

export default AppRouter;
