import React from 'react';
import { useParams } from 'react-router';
import AlbumDetails from '../containers/album-details/AlbumDetails';
import BackgroundBox from '../components/background-box/BackgroundBox';

const AlbumDetailsView: React.SFC = () => {
  let { id } = useParams();

  return (
    <BackgroundBox>
      <AlbumDetails albumId={id!} />
    </BackgroundBox>
  );
};

export default AlbumDetailsView;
