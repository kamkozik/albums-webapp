import React from 'react';
import { useParams } from 'react-router';
import BackgroundBox from '../components/background-box/BackgroundBox';
import UsersDetails from '../containers/users-details/UsersDetails';

const UserDetailsView: React.SFC = () => {
  const { id } = useParams();

  return (
    <BackgroundBox>
      <UsersDetails userId={id!} />
    </BackgroundBox>
  );
};

export default UserDetailsView;
