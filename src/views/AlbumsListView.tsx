import React from 'react';
import AlbumsList from '../containers/albums-list/AlbumsList';
import BackgroundBox from '../components/background-box/BackgroundBox';

const AlbumsListView: React.SFC = () => {
  return (
    <BackgroundBox>
      <AlbumsList />
    </BackgroundBox>
  );
};

export default AlbumsListView;
