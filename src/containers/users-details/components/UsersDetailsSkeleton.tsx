import React from 'react';
import { createStyles, makeStyles } from '@material-ui/styles';
import { Theme, Divider, Box } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    divider: {
      marginBottom: theme.spacing(1)
    }
  })
);

const UsersDetailsSkeletons: React.SFC = () => {
  const classes = useStyles();

  return (
    <Grid container spacing={5}>
      <Grid item xs={12} sm={6}>
        <Paper>
          <Box padding={3}>
            {[...new Array(5)].map((_, index) => (
              <Box key={index}>
                <Skeleton height={35} width="40%" />
                <Skeleton height={20} width="60%" />
                <Divider className={classes.divider} />
              </Box>
            ))}
          </Box>
        </Paper>
      </Grid>
      <Grid item xs={12} sm={6}>
        <Paper>
          <Box padding={3}>
            {[...new Array(4)].map((_, index) => (
              <Box key={index}>
                <Skeleton height={35} width="40%" />
                <Skeleton height={20} width="60%" />
                <Divider className={classes.divider} />
              </Box>
            ))}
          </Box>
        </Paper>
      </Grid>
      <Grid item xs={12} sm={6}>
        <Paper>
          <Box padding={3}>
            {[...new Array(3)].map((_, index) => (
              <Box key={index}>
                <Skeleton height={35} width="40%" />
                <Skeleton height={20} width="60%" />
                <Divider className={classes.divider} />
              </Box>
            ))}
          </Box>
        </Paper>
      </Grid>
    </Grid>
  );
};

export default UsersDetailsSkeletons;
