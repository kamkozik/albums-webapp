import React from 'react';
import { Box, Typography, Divider } from '@material-ui/core';

interface UsersDetailsRowProps {
  title?: string;
  text?: string;
  secondaryText?: string;
  isLast?: boolean;
}

const UsersDetailsRow: React.SFC<UsersDetailsRowProps> = ({
  title,
  text,
  secondaryText,
  isLast
}) => {
  return (
    <Box marginBottom={isLast ? 0 : 1}>
      <Typography color="primary" variant="h6">
        {title}
      </Typography>
      <Typography>{text}</Typography>
      {secondaryText && <Typography>{secondaryText}</Typography>}
      {!isLast && <Divider />}
    </Box>
  );
};

export default UsersDetailsRow;
