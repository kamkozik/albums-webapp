import React, { useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { RootState } from '../../store/rootReducer';
import Box from '@material-ui/core/Box';

import { UserDetailsActions } from './+redux';
import GoBackButton from '../../components/navigation-button/GoBackButton';
import { Typography, Avatar, Paper, Grid, Theme } from '@material-ui/core';
import UsersDetailsRow from './components/UsersDetailsRow';
import UsersDetailsSkeletons from './components/UsersDetailsSkeleton';
import { makeStyles, createStyles } from '@material-ui/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    header: {
      lineHeight: 2.4
    }
  })
);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
  userId: string;
};

const UserDetails: React.SFC<Props> = ({
  loading,
  userId,
  fetchUserDetails,
  userDetails
}: Props) => {
  const classes = useStyles();

  useEffect(() => {
    fetchUserDetails(userId);
  }, [fetchUserDetails, userId]);

  return (
    <Box>
      <Box display="flex" width={1} justifyContent="space-between" marginBottom={2}>
        <GoBackButton />
        <Typography className={classes.header} variant="h6" component="p">
          {userDetails.name}
        </Typography>
        <Avatar>{userDetails.name && userDetails.name.slice(0, 2)}</Avatar>
      </Box>
      {!loading ? (
        <Grid container spacing={5}>
          <Grid item xs={12} sm={6}>
            <Paper>
              <Box padding={3}>
                <UsersDetailsRow title="City" text={userDetails.address.city} />
                <UsersDetailsRow title="Street" text={userDetails.address.street} />
                <UsersDetailsRow title="Suite" text={userDetails.address.suite} />
                <UsersDetailsRow title="Zipcode" text={userDetails.address.zipcode} />
                <UsersDetailsRow
                  isLast
                  title="Coordinates"
                  text={userDetails.address.geo.lat}
                  secondaryText={userDetails.address.geo.lng}
                />
              </Box>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper>
              <Box padding={3}>
                <UsersDetailsRow title="Email" text={userDetails.email} />
                <UsersDetailsRow title="Phone" text={userDetails.phone} />
                <UsersDetailsRow title="Username" text={userDetails.username} />
                <UsersDetailsRow title="Website" text={userDetails.website} isLast />
              </Box>
            </Paper>
          </Grid>

          <Grid item xs={12} sm={6}>
            <Paper>
              <Box padding={3}>
                <UsersDetailsRow title="Company Name" text={userDetails.company.name} />
                <UsersDetailsRow title="Description" text={userDetails.company.catchPhrase} />
                <UsersDetailsRow isLast title="BS" text={userDetails.company.bs} />
              </Box>
            </Paper>
          </Grid>
        </Grid>
      ) : (
        <UsersDetailsSkeletons />
      )}
    </Box>
  );
};

const mapState = (state: RootState) => ({
  loading: state.userDetails.loading,
  userDetails: state.userDetails.userDetails
});

const mapDispatch = (dispatch: ThunkDispatch<any, any, AnyAction>) => {
  return {
    fetchUserDetails: (userId: string) => {
      dispatch(UserDetailsActions.fetchUserDetails(userId));
    }
  };
};

const connector = connect(mapState, mapDispatch);

export default connector(UserDetails);
