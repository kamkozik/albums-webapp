import { User } from '../../../models/User.model';

export const FETCH_USER_DETAILS = '[USER DETAILS] FETCH USER DETAILS';
export const FETCH_USER_DETAILS_SUCCESS = '[USER DETAILS] FETCH USER DETAILS SUCCESS';
export const FETCH_USER_DETAILS_FAILURE = '[USER DETAILS] FETCH USER DETAILS FAILURE';

interface FetchUserDetailsAction {
  type: typeof FETCH_USER_DETAILS;
  payload: string;
}

interface FetchUserDetailsSuccessAction {
  type: typeof FETCH_USER_DETAILS_SUCCESS;
  payload: User;
}

interface FetchUserDetailsFailureAction {
  type: typeof FETCH_USER_DETAILS_FAILURE;
}

export type UserDetailsTypes =
  | FetchUserDetailsAction
  | FetchUserDetailsSuccessAction
  | FetchUserDetailsFailureAction;
