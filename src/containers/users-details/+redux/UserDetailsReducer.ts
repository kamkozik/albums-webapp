import {
  UserDetailsTypes,
  FETCH_USER_DETAILS,
  FETCH_USER_DETAILS_FAILURE,
  FETCH_USER_DETAILS_SUCCESS
} from './UserDetailsTypes';
import { User } from '../../../models/User.model';

export interface UserDetailsState {
  loading: boolean;
  userDetails: User;
}

const initialState: UserDetailsState = {
  loading: false,
  userDetails: {
    address: {
      geo: {}
    },
    company: {}
  }
};

export function userDetailsReducer(
  state = initialState,
  action: UserDetailsTypes
): UserDetailsState {
  switch (action.type) {
    case FETCH_USER_DETAILS:
      return {
        ...state,
        loading: true
      };
    case FETCH_USER_DETAILS_SUCCESS:
      return {
        ...state,
        userDetails: action.payload,
        loading: false
      };
    case FETCH_USER_DETAILS_FAILURE:
      return {
        ...state,
        loading: false
      };
    default:
      return state;
  }
}
