import { ThunkAction } from 'redux-thunk';
import { Action } from 'redux';
import { UserDetailsService } from '../../../services';
import { RootState } from '../../../store/rootReducer';
import {
  UserDetailsTypes,
  FETCH_USER_DETAILS_SUCCESS,
  FETCH_USER_DETAILS_FAILURE,
  FETCH_USER_DETAILS
} from './UserDetailsTypes';
import { User } from '../../../models/User.model';

function fetchUserDetailsCreator(userId: string): UserDetailsTypes {
  return {
    type: FETCH_USER_DETAILS,
    payload: userId
  };
}

function fetchUserDetailsSuccessCreator(userDetails: User): UserDetailsTypes {
  return {
    type: FETCH_USER_DETAILS_SUCCESS,
    payload: userDetails
  };
}

function fetchUserDetailsFailureCreator(): UserDetailsTypes {
  return {
    type: FETCH_USER_DETAILS_FAILURE
  };
}

const fetchUserDetails = (
  userId: string
): ThunkAction<void, RootState, null, Action<string>> => async dispatch => {
  try {
    dispatch(fetchUserDetailsCreator(userId));
    const userDetails = await UserDetailsService.fetchUsersDetails(userId);
    setTimeout(() => {
      dispatch(fetchUserDetailsSuccessCreator(userDetails[0]));
    }, 1000);
  } catch (error) {
    dispatch(fetchUserDetailsFailureCreator());
  }
};

const UserDetailsActions = {
  fetchUserDetails
};

export default UserDetailsActions;
