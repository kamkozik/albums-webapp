import UserDetailsActions from './UserDetailsActions';
import { userDetailsReducer } from './UserDetailsReducer';

export { UserDetailsActions, userDetailsReducer };
