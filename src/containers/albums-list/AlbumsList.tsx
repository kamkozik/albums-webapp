import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { AlbumsListActions } from './+redux';
import { AnyAction } from 'redux';
import { RootState } from '../../store/rootReducer';
import { createStyles, makeStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import { Theme } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import AlbumsListSkeletons from './components/AlbumsListSkeletons';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      height: 70,
      cursor: 'pointer',
      padding: theme.spacing(1),
      display: 'flex',
      alignItems: 'center'
    },
    link: {
      textDecoration: 'none'
    }
  })
);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {};

const AlbumsList: React.SFC<Props> = ({
  fetchAlbumsList,
  albumsList,
  loading
}: Props) => {
  const classes = useStyles();
  useEffect(() => {
    albumsList.length === 0 && fetchAlbumsList();
  }, [fetchAlbumsList, albumsList]);
  return (
    <Box>
      <Grid container spacing={3}>
        {!loading ? (
          albumsList.map(album => (
            <Grid key={album.id} item xs={12} md={6} lg={4}>
              <Link className={classes.link} to={`album/${album.id}`}>
                <Paper className={classes.paper}>
                  <Typography>{album.title}</Typography>
                </Paper>
              </Link>
            </Grid>
          ))
        ) : (
          <AlbumsListSkeletons />
        )}
      </Grid>
    </Box>
  );
};

const mapState = (state: RootState) => ({
  albumsList: state.albumsList.list,
  loading: state.albumsList.loading
});

const mapDispatch = (dispatch: ThunkDispatch<any, any, AnyAction>) => {
  return {
    fetchAlbumsList: () => {
      dispatch(AlbumsListActions.fetchAlbumsList());
    }
  };
};

const connector = connect(mapState, mapDispatch);

export default connector(AlbumsList);
