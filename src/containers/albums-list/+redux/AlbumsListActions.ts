import {
  AlbumsListTypes,
  FETCH_ALBUMS_LIST,
  FETCH_ALBUMS_LIST_SUCCESS,
  FETCH_ALBUMS_LIST_FAILURE
} from './AlbumsListTypes';
import { ThunkAction } from 'redux-thunk';
import { Action } from 'redux';
import { AlbumsListService } from '../../../services';
import { RootState } from '../../../store/rootReducer';
import Album from '../../../models/AlbumsList.model';

function fetchAlbumsListCreator(): AlbumsListTypes {
  return {
    type: FETCH_ALBUMS_LIST
  };
}

function fetchAlbumsListSuccessCreator(
  albumsList: Array<Album>
): AlbumsListTypes {
  return {
    type: FETCH_ALBUMS_LIST_SUCCESS,
    payload: albumsList
  };
}

function fetchAlbumsListFailureCreator(): AlbumsListTypes {
  return {
    type: FETCH_ALBUMS_LIST_FAILURE
  };
}

const fetchAlbumsList = (): ThunkAction<
  void,
  RootState,
  null,
  Action<string>
> => async dispatch => {
  try {
    dispatch(fetchAlbumsListCreator());
    const albumsList = await AlbumsListService.fetchAlbumsList();
    setTimeout(() => {
      dispatch(fetchAlbumsListSuccessCreator(albumsList));
    }, 1000);
  } catch (error) {
    dispatch(fetchAlbumsListFailureCreator());
  }
};

const AlbumsListActions = {
  fetchAlbumsList
};

export default AlbumsListActions;
