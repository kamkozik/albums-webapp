import {
  AlbumsListTypes,
  FETCH_ALBUMS_LIST,
  FETCH_ALBUMS_LIST_SUCCESS,
  FETCH_ALBUMS_LIST_FAILURE
} from './AlbumsListTypes';
import Album from '../../../models/AlbumsList.model';

export interface AlbumsListState {
  list: Array<Album>;
  loading: boolean;
}

const initialState: AlbumsListState = {
  list: [],
  loading: false
};

export function albumsListReducer(
  state = initialState,
  action: AlbumsListTypes
): AlbumsListState {
  switch (action.type) {
    case FETCH_ALBUMS_LIST:
      return {
        ...state,
        loading: true
      };
    case FETCH_ALBUMS_LIST_SUCCESS:
      return {
        ...state,
        list: action.payload,
        loading: false
      };
    case FETCH_ALBUMS_LIST_FAILURE:
      return {
        ...state,
        loading: false
      };
    default:
      return state;
  }
}
