import Album from '../../../models/AlbumsList.model';

export const FETCH_ALBUMS_LIST = '[ALBUMS LIST] FETCH ALBUMS LIST';
export const FETCH_ALBUMS_LIST_SUCCESS =
  '[ALBUMS LIST] FETCH ALBUMS LIST SUCCESS';
export const FETCH_ALBUMS_LIST_FAILURE =
  '[ALBUMS LIST] FETCH ALBUMS LIST FAILURE';

interface FetchAlbumsListAction {
  type: typeof FETCH_ALBUMS_LIST;
}

interface FetchAlbumsListSuccessAction {
  type: typeof FETCH_ALBUMS_LIST_SUCCESS;
  payload: Array<Album>;
}

interface FetchAlbumsListFailureAction {
  type: typeof FETCH_ALBUMS_LIST_FAILURE;
}

export type AlbumsListTypes =
  | FetchAlbumsListAction
  | FetchAlbumsListSuccessAction
  | FetchAlbumsListFailureAction;
