import React, { Fragment } from 'react';
import { createStyles, makeStyles } from '@material-ui/styles';
import { Theme } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      height: 70,
      padding: theme.spacing(1),
      display: 'flex',
      alignItems: 'center'
    }
  })
);

const AlbumsListSkeletons: React.SFC = () => {
  const classes = useStyles();

  return (
    <Fragment>
      {[...new Array(10)].map((_, index) => (
        <Grid key={index} item xs={12} md={6} lg={4}>
          <Paper className={classes.paper}>
            <Skeleton width="60%" height={20} />
          </Paper>
        </Grid>
      ))}
    </Fragment>
  );
};

export default AlbumsListSkeletons;
