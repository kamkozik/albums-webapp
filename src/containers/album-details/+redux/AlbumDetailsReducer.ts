import AlbumPhoto from '../../../models/AlbumPhoto.model';
import {
  AlbumDetailsTypes,
  FETCH_PHOTOS_LIST,
  FETCH_PHOTOS_LIST_SUCCESS,
  FETCH_PHOTOS_LIST_FAILURE,
  FETCH_ALBUM_DETAILS_SUCCESS,
  FETCH_ALBUM_DETAILS,
  FETCH_ALBUM_DETAILS_FAILURE
} from './AlbumDetailsTypes';
import Album from '../../../models/AlbumsList.model';

export interface AlbumDetailsState {
  list: Array<AlbumPhoto>;
  loading: boolean;
  loadingDetails: boolean;
  albumDetails: Album | undefined;
}

const initialState: AlbumDetailsState = {
  list: [],
  loading: false,
  loadingDetails: false,
  albumDetails: undefined
};

export function albumDetailsReducer(
  state = initialState,
  action: AlbumDetailsTypes
): AlbumDetailsState {
  switch (action.type) {
    case FETCH_PHOTOS_LIST:
      return {
        ...state,
        loading: true
      };
    case FETCH_PHOTOS_LIST_SUCCESS:
      return {
        ...state,
        list: action.payload,
        loading: false
      };
    case FETCH_PHOTOS_LIST_FAILURE:
      return {
        ...state,
        loading: false
      };
    case FETCH_ALBUM_DETAILS:
      return {
        ...state,
        loadingDetails: true
      };
    case FETCH_ALBUM_DETAILS_SUCCESS:
      return {
        ...state,
        albumDetails: action.payload,
        loadingDetails: false
      };
    case FETCH_ALBUM_DETAILS_FAILURE:
      return {
        ...state,
        loadingDetails: false
      };
    default:
      return state;
  }
}
