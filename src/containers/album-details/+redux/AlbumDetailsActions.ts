import { ThunkAction } from 'redux-thunk';
import { Action } from 'redux';
import { AlbumDetailsService } from '../../../services';
import { RootState } from '../../../store/rootReducer';
import {
  AlbumDetailsTypes,
  FETCH_PHOTOS_LIST,
  FETCH_PHOTOS_LIST_SUCCESS,
  FETCH_PHOTOS_LIST_FAILURE,
  FETCH_ALBUM_DETAILS,
  FETCH_ALBUM_DETAILS_SUCCESS,
  FETCH_ALBUM_DETAILS_FAILURE
} from './AlbumDetailsTypes';
import AlbumPhoto from '../../../models/AlbumPhoto.model';
import Album from '../../../models/AlbumsList.model';

function fetchPhotosListCreator(albumId: string): AlbumDetailsTypes {
  return {
    type: FETCH_PHOTOS_LIST,
    payload: albumId
  };
}

function fetchPhotosListSuccessCreator(photosList: Array<AlbumPhoto>): AlbumDetailsTypes {
  return {
    type: FETCH_PHOTOS_LIST_SUCCESS,
    payload: photosList
  };
}

function fetchPhotosListFailureCreator(): AlbumDetailsTypes {
  return {
    type: FETCH_PHOTOS_LIST_FAILURE
  };
}

const fetchPhotosList = (
  albumId: string
): ThunkAction<void, RootState, null, Action<string>> => async dispatch => {
  try {
    dispatch(fetchPhotosListCreator(albumId));
    const photosList = await AlbumDetailsService.fetchPhotosList(albumId);
    setTimeout(() => {
      dispatch(fetchPhotosListSuccessCreator(photosList));
    }, 1000);
  } catch (error) {
    dispatch(fetchPhotosListFailureCreator());
  }
};

function fetchAlbumDetailsCreator(id: string): AlbumDetailsTypes {
  return {
    type: FETCH_ALBUM_DETAILS,
    payload: id
  };
}

function fetchAlbumDetailsCreatorSuccessCreator(albumDetails: Album): AlbumDetailsTypes {
  return {
    type: FETCH_ALBUM_DETAILS_SUCCESS,
    payload: albumDetails
  };
}

function fetchAlbumDetailsCreatorFailureCreator(): AlbumDetailsTypes {
  return {
    type: FETCH_ALBUM_DETAILS_FAILURE
  };
}

const fetchAlbumDetails = (
  id: string
): ThunkAction<void, RootState, null, Action<string>> => async dispatch => {
  try {
    dispatch(fetchAlbumDetailsCreator(id));
    const albumDetails = await AlbumDetailsService.fetchAlbumDetails(id);
    setTimeout(() => {
      dispatch(fetchAlbumDetailsCreatorSuccessCreator(albumDetails[0]));
    }, 500);
  } catch (error) {
    dispatch(fetchAlbumDetailsCreatorFailureCreator());
  }
};

const AlbumDetailsActions = {
  fetchPhotosList,
  fetchAlbumDetails
};

export default AlbumDetailsActions;
