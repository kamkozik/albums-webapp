import AlbumPhoto from '../../../models/AlbumPhoto.model';
import Album from '../../../models/AlbumsList.model';

export const FETCH_PHOTOS_LIST = '[ALBUM DETAILS] FETCH PHOTOS LIST';
export const FETCH_PHOTOS_LIST_SUCCESS =
  '[ALBUM DETAILS] FETCH PHOTOS LIST SUCCESS';
export const FETCH_PHOTOS_LIST_FAILURE =
  '[ALBUM DETAILS] FETCH PHOTOS LIST FAILURE';
export const FETCH_ALBUM_DETAILS = '[ALBUM DETAILS] FETCH ALBUM DETAILS LIST';
export const FETCH_ALBUM_DETAILS_SUCCESS =
  '[ALBUM DETAILS] FETCH ALBUM DETAILS SUCCESS';
export const FETCH_ALBUM_DETAILS_FAILURE =
  '[ALBUM DETAILS] FETCH ALBUM DETAILS FAILURE';

interface FetchPhotosListAction {
  type: typeof FETCH_PHOTOS_LIST;
  payload: string;
}

interface FetchPhotosListSuccessAction {
  type: typeof FETCH_PHOTOS_LIST_SUCCESS;
  payload: Array<AlbumPhoto>;
}

interface FetchPhotosListFailureAction {
  type: typeof FETCH_PHOTOS_LIST_FAILURE;
}

interface FetchAlbumDetailsAction {
  type: typeof FETCH_ALBUM_DETAILS;
  payload: string;
}

interface FetchAlbumDetailsSuccessAction {
  type: typeof FETCH_ALBUM_DETAILS_SUCCESS;
  payload: Album;
}

interface FetchAlbumDetailsFailureAction {
  type: typeof FETCH_ALBUM_DETAILS_FAILURE;
}

export type AlbumDetailsTypes =
  | FetchPhotosListAction
  | FetchPhotosListSuccessAction
  | FetchPhotosListFailureAction
  | FetchAlbumDetailsAction
  | FetchAlbumDetailsSuccessAction
  | FetchAlbumDetailsFailureAction;
