import React, { useEffect, Fragment } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { RootState } from '../../store/rootReducer';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import AlbumDetailsActions from './+redux/AlbumDetailsActions';
import AlbumDetailsCard from './components/AlbumDetailsCard';
import AlbumDetailsCardSkeleton from './components/AlbumDetailsCardSkeleton';
import GoBackButton from '../../components/navigation-button/GoBackButton';
import { Link } from 'react-router-dom';
import { makeStyles, Theme, createStyles } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    header: {
      lineHeight: 2.4
    }
  })
);

type PropsFromRedux = ConnectedProps<typeof connector>;

type Props = PropsFromRedux & {
  albumId: string;
};

const AlbumDetails: React.SFC<Props> = ({
  loading,
  loadingDetails,
  albumId,
  photosList,
  albumDetails,
  fetchPhotosList,
  fetchAlbumDetails
}: Props) => {
  const classes = useStyles();
  useEffect(() => {
    fetchPhotosList(albumId);
  }, [fetchPhotosList, albumId]);
  useEffect(() => {
    fetchAlbumDetails(albumId);
  }, [fetchAlbumDetails, albumId]);
  return (
    <Box>
      <Box display="flex" width={1} justifyContent="space-between" marginBottom={2}>
        <GoBackButton />
        {!loadingDetails ? (
          <Fragment>
            <Typography className={classes.header} variant="h6" component="p">
              {albumDetails && albumDetails.title}
            </Typography>
            <Tooltip title="Go to user details">
              <Link
                replace
                to={{
                  pathname: `../../user/${albumDetails && albumDetails.userId}`
                }}
              >
                <IconButton edge="start" color="default" aria-label="go to user details">
                  <AccountCircleIcon />
                </IconButton>
              </Link>
            </Tooltip>
          </Fragment>
        ) : (
          <Fragment>
            <Skeleton width="40%" />
            <Skeleton variant="circle" width={40} height={40} />
          </Fragment>
        )}
      </Box>
      <Grid container spacing={3}>
        {!loading ? (
          photosList.map((photo, index) => (
            <Grid key={index} item xs={12} md={6} lg={4}>
              <AlbumDetailsCard
                thumbnailUrl={photo.thumbnailUrl}
                title={photo.title}
                url={photo.url}
              />
            </Grid>
          ))
        ) : (
          <AlbumDetailsCardSkeleton />
        )}
      </Grid>
    </Box>
  );
};

const mapState = (state: RootState) => ({
  photosList: state.albumDetails.list,
  loading: state.albumDetails.loading,
  loadingDetails: state.albumDetails.loadingDetails,
  albumDetails: state.albumDetails.albumDetails
});

const mapDispatch = (dispatch: ThunkDispatch<any, any, AnyAction>) => {
  return {
    fetchPhotosList: (albumId: string) => {
      dispatch(AlbumDetailsActions.fetchPhotosList(albumId));
    },
    fetchAlbumDetails: (albumId: string) => {
      dispatch(AlbumDetailsActions.fetchAlbumDetails(albumId));
    }
  };
};

const connector = connect(mapState, mapDispatch);

export default connector(AlbumDetails);
