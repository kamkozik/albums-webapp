import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core';
import Modal from '@material-ui/core/Modal';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    }
  })
);

interface AlbumDetailsCardProps {
  onClose: (event: React.MouseEvent<HTMLAnchorElement>) => void;
  open: boolean;
  url: string;
}

const PhotoPreviewModal: React.SFC<AlbumDetailsCardProps> = ({ open, url, onClose }) => {
  const classes = useStyles();
  return (
    <Modal className={classes.modal} open={open} onClose={onClose}>
      <img src={url} alt="album preview" />
    </Modal>
  );
};

export default PhotoPreviewModal;
