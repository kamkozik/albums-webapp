import React, { useState, Fragment } from 'react';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AspectRatioIcon from '@material-ui/icons/AspectRatio';
import { makeStyles, Theme, createStyles } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import PhotoPreviewModal from './PhotoPreviewModal';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    cardMedia: {
      height: 200
    },
    cardActions: {
      height: 40
    },
    content: {
      width: '100%'
    }
  })
);

interface AlbumDetailsCardProps {
  thumbnailUrl: string;
  title: string;
  url: string;
}

const AlbumDetailsCard: React.FC<AlbumDetailsCardProps> = ({ thumbnailUrl, title, url }) => {
  const classes = useStyles();
  const [openPreviewModal, setOpenPreviewModal] = useState(false);
  const toggleDrawer = () => {
    setOpenPreviewModal(!openPreviewModal);
  };
  return (
    <Fragment>
      <Card>
        <CardHeader
          classes={{ content: classes.content }}
          title={<Typography noWrap>{title}</Typography>}
        />
        <CardMedia className={classes.cardMedia} image={thumbnailUrl} />
        <CardActions className={classes.cardActions} disableSpacing>
          <IconButton onClick={toggleDrawer} aria-label="add to favorites">
            <AspectRatioIcon />
          </IconButton>
        </CardActions>
      </Card>
      <PhotoPreviewModal open={openPreviewModal} url={url} onClose={toggleDrawer} />
    </Fragment>
  );
};

export default AlbumDetailsCard;
