import React, { Fragment } from 'react';
import { createStyles, makeStyles } from '@material-ui/styles';
import { Theme, Grid } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    cardMedia: {
      height: 200
    },
    cardActions: {
      height: 40
    }
  })
);

const AlbumDetailsCardSkeleton: React.SFC = () => {
  const classes = useStyles();

  return (
    <Fragment>
      {[...new Array(5)].map((val, index) => (
        <Grid key={index} item xs={12} md={6} lg={4}>
          <Card>
            <CardHeader title={<Skeleton width="60%" height={20} />} />
            <Skeleton className={classes.cardMedia} variant="rect" />
            <CardActions className={classes.cardActions} disableSpacing>
              <Skeleton variant="circle" width={30} height={30} />
            </CardActions>
          </Card>
        </Grid>
      ))}
    </Fragment>
  );
};

export default AlbumDetailsCardSkeleton;
