import React from 'react';
import './App.css';
import AppRouter from './router/AppRouter';
import { ThemeProvider } from '@material-ui/core';
import MainTheme from './themes/MainTheme';

const App: React.FC = () => {
  return (
    <ThemeProvider theme={MainTheme}>
      <AppRouter />
    </ThemeProvider>
  );
};

export default App;
